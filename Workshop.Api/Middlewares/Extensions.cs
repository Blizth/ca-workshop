﻿using System.Text.Json;
using Workshop.Infrastructure.Exceptions;

namespace Workshop.Api.Middlewares
{
    public static class Extensions
    {
        public static IServiceCollection AddErrorHandler<T>(this IServiceCollection services,
           JsonSerializerOptions serializerOptions = null)
           where T : class, IExceptionToResponseMapper
        {

            serializerOptions ??= new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            services
                .AddSingleton(serializerOptions)
                .AddTransient<ExceptionHandlerMiddleware>()
                .AddScoped<IExceptionToResponseMapper, T>();

            return services;
        }
    }
}
