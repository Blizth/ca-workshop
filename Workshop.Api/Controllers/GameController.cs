﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Workshop.Application.Commands.AddGame;
using Workshop.Application.Dtos;
using Workshop.Application.Queries.GetGames;

namespace Workshop.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : Controller
    {
        private readonly IMediator _mediator;

        public GameController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Get all Games
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns all game records</response>
        [HttpGet]
        [ProducesResponseType(typeof(IList<GameDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var result = await _mediator.Send(new GetGames());
            return Ok(result);
        }

        /// <summary>
        /// Add new Game
        /// </summary>
        /// <param name="addGame"></param>
        /// <returns></returns>
        /// <response code="201">Created resource</response>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status201Created)]
        public async Task<IActionResult> Post([FromBody]AddGame addGame)
        {
            var result = await _mediator.Send(addGame);
            return CreatedAtAction(nameof(Post), result);
        }

        /// <summary>
        /// Delete Game
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns all game records</response>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return Ok();
        }

        /// <summary>
        /// Add new Game
        /// </summary>
        /// <param name="addGame"></param>
        /// <returns></returns>
        /// <response code="201">Created resource</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return Ok();
        }
    }
}
