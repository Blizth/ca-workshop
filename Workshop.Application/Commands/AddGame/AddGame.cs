﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Dtos;

namespace Workshop.Application.Commands.AddGame
{
    public class AddGame : IRequest<GameDto>
    {
        public Guid Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description {  get; set; } = string.Empty;
        public DateTime ReleaseDate { get; set; }
        public IList<string> Tags {  get; set; } = new List<string>();

    }
}
