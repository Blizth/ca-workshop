﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Dtos;
using Workshop.Application.Exceptions;
using Workshop.Core.Entities;
using Workshop.Core.Repositories;

namespace Workshop.Application.Commands.AddGame
{
    internal class AddGameHandler : IRequestHandler<AddGame, GameDto>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMapper _mapper;

        public AddGameHandler(IGameRepository gameRepository, IMapper mapper)
        {
            _gameRepository = gameRepository;
            _mapper = mapper;
        }

        async Task<GameDto> IRequestHandler<AddGame, GameDto>.Handle(AddGame request, CancellationToken cancellationToken)
        {
            if (await _gameRepository.ExistsAsync(request.Id))
            {
                throw new GameAlreadyExistsException(request.Id);
            }

            var game = Game.Create(request.Id, request.Title, request.Description, request.ReleaseDate, request.Tags);
            await _gameRepository.AddAsync(game);
            return _mapper.Map<GameDto>(game);
        }
    }
}
