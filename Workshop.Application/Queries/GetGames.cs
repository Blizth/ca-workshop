﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Dtos;

namespace Workshop.Application.Queries.GetGames
{
    public class GetGames : IRequest<IList<GameDto>>
    {
    }
}
