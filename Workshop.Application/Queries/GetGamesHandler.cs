﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Dtos;
using Workshop.Core.Repositories;

namespace Workshop.Application.Queries.GetGames
{
    public class GetGamesHandler : IRequestHandler<GetGames, IList<GameDto>>
    {
        private readonly IGameRepository _gameRepository;
        private readonly IMapper _mapper;

        public GetGamesHandler(IGameRepository gameRepository, IMapper mapper)
        {
            _gameRepository = gameRepository;
            _mapper = mapper;
        }

        public async Task<IList<GameDto>> Handle(GetGames request, CancellationToken cancellationToken)
        {
            var result = await _gameRepository.GetCollectionAsync();
            var afterMapping = _mapper.Map<IList<GameDto>>(result);
            return afterMapping;
        }
    }
}
