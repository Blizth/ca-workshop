﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Application.Dtos
{
    public class GameDto
    {
        public Guid Id { get; set; }
        public string Title { get; init; } = String.Empty;
        public string Description { get; init; } = String.Empty;
        public DateTime ReleaseDay { get; init; }
        public IEnumerable<string> Tags { get; init; } = new List<string>();
    }
}
