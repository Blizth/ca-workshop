﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Application.Exceptions
{
    public class AppException : Exception
    {
        public virtual string Code { get; } = string.Empty;

        protected AppException(string message) : base(message)
        {
        }
    }
}
