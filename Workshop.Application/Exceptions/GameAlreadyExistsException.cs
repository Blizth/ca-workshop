﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Application.Exceptions
{
    internal class GameAlreadyExistsException : AppException
    {
        public Guid GameId { get; private set; }
        public override string Code { get; } = "game_id_already_exists";

        internal GameAlreadyExistsException(Guid id) : base($"Game with id {id} already exists") => GameId = id;
    }
}
