using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Workshop.Core.Entities;
using Workshop.Core.Events;
using Workshop.Core.Exceptions;
using Xunit;

namespace Workshop.Unit
{
    public class CreateGameTest
    {
        //Act
        private Game Act(Guid id, string title, string description, DateTime releaseDate, IEnumerable<string> tags)
            => Game.Create(id, title, description, releaseDate, tags);

        [Fact]
        public void game_after_create_should_create_event()
        {
            //Arrange
            var id = new AggregateId(Guid.NewGuid());
            var title = "Random title";
            var description = "Random desc";
            var releaseDate = new DateTime();
            var tags = new List<string>() { "tag1" };

            //Act
            var game = Act(id, title, description, releaseDate, tags);

            //Assert
            game.ShouldNotBeNull();
            game.Id.ShouldBe(id);
            game.Title.ShouldBe(title);
            game.Description.ShouldBe(description);
            game.ReleaseDay.ShouldBe(releaseDate);
            game.Tags.ShouldBe(tags);
            game.Events.Count().ShouldBe(1);

            var @event = game.Events.Single();
            @event.ShouldBeOfType<GameAdded>();

        }

        [Fact]
        public void game_after_create_with_empty_title_should_throw_an_exception()
        {
            //Arrange
            var id = new AggregateId(Guid.NewGuid());
            var title = "";
            var description = "Random desc";
            var releaseDate = new DateTime();
            var tags = new List<string>() { "tag1" };

            //Act
            var exception = Record.Exception(() => Act(id, title, description, releaseDate, tags));

            //Assert
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<EmptyTitleException>();
        }

        [Fact]
        public void game_after_create_with_no_tags_should_throw_an_exception()
        {
            //Arrange
            var id = new AggregateId(Guid.NewGuid());
            var title = "Random title";
            var description = "Random desc";
            var releaseDate = new DateTime();
            var tags = new List<string>();

            //Act
            var exception = Record.Exception(() => Act(id, title, description, releaseDate, tags));

            //Assert
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<NoTagsException>();
        }

        [Theory]
        [InlineData("", "elem")]
        [InlineData("", "")]
        [InlineData("elem", "")]
        public void game_after_create_with_empty_tag_should_throw_an_exception(string tag1, string tag2)
        {
            //Arrange
            var id = new AggregateId(Guid.NewGuid());
            var title = "Random title";
            var description = "Random desc";
            var releaseDate = new DateTime();
            var tags = new List<string>() { tag1, tag2 };

            //Act
            var exception = Record.Exception(() => Act(id, title, description, releaseDate, tags));

            //Assert
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<EmptyTagException>();
        }
    }
}