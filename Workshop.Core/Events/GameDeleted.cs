﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Entities;

namespace Workshop.Core.Events
{
    internal class GameDeleted : IDomainEvent
    {
        public Game Game { get; private set; }

        public GameDeleted(Game game) => Game = game;
    }
}
