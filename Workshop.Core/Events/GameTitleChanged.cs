﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Entities;

namespace Workshop.Core.Events
{
    internal class GameTitleChanged : IDomainEvent
    {
        public Game Game { get; private set; }

        public string Title { get; private set; }

        public GameTitleChanged(Game game, string title) => (Game, Title) = (game, title);
    }
}
