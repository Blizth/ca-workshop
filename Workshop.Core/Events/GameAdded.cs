﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Entities;

namespace Workshop.Core.Events
{
    public class GameAdded : IDomainEvent
    {
        public Game Game { get; private set; }

        public GameAdded(Game game) => Game = game;
    }
}
