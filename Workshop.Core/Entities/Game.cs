﻿using Workshop.Core.Events;
using Workshop.Core.Exceptions;

namespace Workshop.Core.Entities
{
    public class Game : AggregateRoot
    {
        private string _title = string.Empty;
        private string _description = string.Empty;
        private DateTime _releaseDay;
        private ISet<string> _tags = new HashSet<string>();

        public IEnumerable<string> Tags
        {
            get => _tags;
            private set => _tags = new HashSet<string>(value);
        }

        public string Title { get => _title; private set => _title = value; }
        public string Description { get => _description; private set => _description = value; }
        public DateTime ReleaseDay { get => _releaseDay; private set => _releaseDay = value; }


        public Game(Guid id, string title, string description, DateTime releaseDay, IEnumerable<string> tags)
        {
            ValidateTags(tags);
            ValidateTitle(title);

            Id = id;
            Title = title;
            Description = description;
            ReleaseDay = releaseDay;
            Tags = tags;  
        }

        private static void ValidateTags(IEnumerable<string> tags)
        {
            if (tags is null || !tags.Any())
            {
                throw new NoTagsException();
            }

            if (tags.Any(string.IsNullOrWhiteSpace))
            {
                throw new EmptyTagException();
            }
        }

        private static void ValidateTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new EmptyTitleException();
            }
        }

        public static Game Create(Guid id, string title, string description, DateTime releaseDay, IEnumerable<string> tags)
        {
            var game = new Game(id, title, description, releaseDay, tags);
            game.AddEvent(new GameAdded(game));
            return game;
        }

        public void ChangeGameTitle(string title)
        {
            ValidateTitle(title);
            Title = title;
            this.AddEvent(new GameTitleChanged(this, title));
        }

        public void DeleteGame()
        {            
            this.AddEvent(new GameDeleted(this));
        }
    }
}
