﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Entities;

namespace Workshop.Core.Repositories
{
    public interface IGameRepository
    {
        Task<Game> GetAsync(AggregateId id);
        Task<IList<Game>> GetCollectionAsync();
        Task<bool> ExistsAsync(AggregateId id);
        Task AddAsync(Game game);
        Task DeleteAsync(AggregateId id);
    }
}
