﻿namespace Workshop.Core.Exceptions
{
    public class EmptyTagException : DomainException
    {
        public override string Code { get; } = "empty_tag";

        public EmptyTagException() : base($"One of the tags is empty")
        { }
    }
}
