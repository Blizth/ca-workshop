﻿namespace Workshop.Core.Exceptions
{
    public class NoTagsException : DomainException
    {
        public override string Code { get; } = "no_tags";

        public NoTagsException() : base($"Tags array cannot be empty")
        { }
    }
}
