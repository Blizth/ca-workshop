﻿namespace Workshop.Core.Exceptions
{
    public class EmptyTitleException : DomainException
    {
        public override string Code { get; } = "empty_title";

        public EmptyTitleException() : base($"Title cannot be empty")
        { }
    }
}
