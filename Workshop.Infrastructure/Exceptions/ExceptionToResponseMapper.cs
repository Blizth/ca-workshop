﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Exceptions;
using Workshop.Core.Exceptions;

namespace Workshop.Infrastructure.Exceptions;
public class ExceptionToResponseMapper : IExceptionToResponseMapper
{

    public async Task<ExceptionResponse> Map(Exception exception)
        => exception switch
        {
            DomainException ex => new ExceptionResponse(new { code = ex.Code, reason = ex.Message }, HttpStatusCode.BadRequest),
            AppException ex => new ExceptionResponse(new { code = ex.Code, reason = ex.Message }, HttpStatusCode.BadRequest),
            _ => new ExceptionResponse(new { code = "Error", reason = "There was internal error" }, HttpStatusCode.InternalServerError) //jak się wszystko posypie to rzuć generic błędem
        };
}
