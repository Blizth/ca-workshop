﻿namespace Workshop.Infrastructure.Exceptions
{
    public interface IExceptionToResponseMapper
    {
        Task<ExceptionResponse> Map(Exception exception);
    }
}
