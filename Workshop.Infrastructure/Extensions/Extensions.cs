﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Repositories;
using Workshop.Infrastructure.DataProvider.InMemory;
using Workshop.Infrastructure.Exceptions;
using Workshop.Infrastructure.Mapper;
using Workshop.Infrastructure.Repositories;

namespace Workshop.Infrastructure.Extensions
{
    public static class Extensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services
                .AddAutoMapperProfile()
                .AddMediatR(typeof(Extensions).Assembly, typeof(Application.Extensions.Extensions).Assembly)
                .AddSingleton<IMemoryContext, MemoryContext>()
                .AddTransient<IGameRepository, GameRepository>();


            return services;
        }
    }
}
