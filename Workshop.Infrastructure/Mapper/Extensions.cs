﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Application.Dtos;
using Workshop.Core.Entities;
using Workshop.Infrastructure.DataProvider.InMemory;

namespace Workshop.Infrastructure.Mapper
{
    internal static class Extension
    {
        public static IServiceCollection AddAutoMapperProfile(this IServiceCollection services)
            => services.AddAutoMapper(typeof(ModelMapper));
    }

    internal class ModelMapper : Profile
    {
        public ModelMapper()
        {
            CreateMap<GameModel, GameDto>().ReverseMap();
            CreateMap<Game, GameDto>().ReverseMap();
            CreateMap<Game, GameModel>().ReverseMap();
        }
    }
}
