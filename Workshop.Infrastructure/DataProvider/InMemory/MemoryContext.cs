﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Infrastructure.DataProvider.InMemory
{
    public class MemoryContext : IMemoryContext
    {
        private IList<GameModel> _gameModels = new List<GameModel>()
        {
            new GameModel(Guid.NewGuid(), "Pokemon Midori", "Gotta catch'em all", new DateTime(1996, 2, 27), new List<string>{"jrpg", "adventure" }),
            new GameModel(Guid.NewGuid(), "Portal", "Cake is a lie", new DateTime(2007, 10, 10), new List<string>{"puzzle", "lie" })
        };

        public void AddItem(GameModel item)
        {
            _gameModels.Add(item);
        }

        public bool Exists(Guid id)
        {
            return _gameModels.Any(item => item.Id == id);
        }

        public GameModel GetItem(Guid id) => _gameModels.FirstOrDefault(x => x.Id == id);

        public IList<GameModel> GetItems()
        {
            return _gameModels;
        }

        public void RemoveItem(Guid id)
        {
            _gameModels.Remove(GetItem(id));
        }
    }
}
