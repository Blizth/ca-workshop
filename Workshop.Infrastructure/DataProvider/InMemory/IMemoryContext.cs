﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Infrastructure.DataProvider.InMemory
{
    public interface IMemoryContext
    {
        IList<GameModel> GetItems();
        GameModel GetItem(Guid id);
        void AddItem(GameModel item);
        void RemoveItem(Guid id);
        public bool Exists(Guid id);
    }
}
