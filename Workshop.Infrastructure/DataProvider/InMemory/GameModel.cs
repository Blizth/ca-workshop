﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop.Infrastructure.DataProvider.InMemory
{
    public record GameModel(Guid Id, string Title, string Description, DateTime ReleaseDay, IEnumerable<string> Tags);
}
