﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workshop.Core.Entities;
using Workshop.Core.Repositories;
using Workshop.Infrastructure.DataProvider.InMemory;

namespace Workshop.Infrastructure.Repositories
{
    internal class GameRepository : IGameRepository
    {
        private readonly IMemoryContext _memoryContext;
        private readonly IMapper _mapper;

        public GameRepository(IMemoryContext memoryContext, IMapper mapper)
        {
            _memoryContext = memoryContext;
            _mapper = mapper;
        }

        public Task AddAsync(Game game)
        {
            _memoryContext.AddItem(_mapper.Map<GameModel>(game));
            return Task.CompletedTask;
        }

        public Task DeleteAsync(AggregateId id)
        {
            _memoryContext.RemoveItem(id);
            return Task.CompletedTask; 
        }

        public Task<bool> ExistsAsync(AggregateId id)
        {
            return Task.FromResult(_memoryContext.Exists(id));
        }

        public Task<Game> GetAsync(AggregateId id)
        {
            var game = _mapper.Map<Game>(_memoryContext.GetItem(id));
            return Task.FromResult(game);
        }

        public Task<IList<Game>> GetCollectionAsync()
        {
            var games = _mapper.Map<IList<Game>>(_memoryContext.GetItems());
            return Task.FromResult(games);
        }
    }
}
